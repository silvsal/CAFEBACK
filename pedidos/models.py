from djongo import models
from django.utils import timezone

def current_time():
    return timezone.now().time()

class Pedido(models.Model):
    id = models.IntegerField(primary_key=True)
    mesa = models.IntegerField()
    lista_productos = models.JSONField()
    cliente = models.CharField(max_length=100, default='cliente')
    estado = models.CharField(max_length=50, default='incompleto')    
    # monto = models.DecimalField(default=0, max_digits=10, decimal_places=2)
    # recepcion_hora = models.TimeField(auto_now_add=True)
    recepcioncuando = models.TimeField(default=current_time)
    # recepcion_fecha = models.DateField(auto_now_add=True)
    preparado_hora = models.TimeField(null=True)
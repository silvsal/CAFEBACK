# Bootcamp de Desarrollo Web Full Stack: Modulo de API REST con DRF 
Este proyecto contiene el código de las clases desarrolladas en el modulo de API REST con DRF.  
Sirve como base para la construcción de nuestro proyecto de la API de Cafeteria.

# Requerimientos
Para levantar este proyecto necesitas:
- Python 3+
- MongoDB
- Conocimientos basicos de Django y DRF

## Instalación de dependencias
Para instalar las dependencias del proyecto, siga estos pasos:
1. Abra una terminal dentro del proyecto, en la misma ubicación que el archivo *requirements.txt.*
2. Active el virtualenv del proyecto.
2. Ejecute el siguiente comando pip para instalar las dependencias del proyecto desde el archivo *requirements.txt*:
`pip install -r requirements.txt`

## Correr migraciones
Para crear la base de datos localmente deben correr el siguiente comando
`python .\manage.py migrate`